export function hablarConI(strInicial) {
//throw new Error('Not implemented'); 
    let strChange = []; 
    let strFinal;
    let strLength = strInicial.length;
    const regVocal = /[aeiouAEIOUáéíóúÁÉÍÓÚ]/;
    const conAcentoMinus = /[áéíóú]/;
    const conAcentoMayus = /[ÁÉÍÓÚ]/;
    const sinAcentoMayus = /[AEIOU]/;
    const sinAcentoMinus = /[aeiou]/; 
    
    
    for(let i = 0 ; i < strLength; i++){
         if(regVocal.test(strInicial[i])){
              function changeVocal (vocal) {
                   if(sinAcentoMinus.test(vocal)){
                        return vocal.replace(vocal,'i');
                    }else if (sinAcentoMayus.test(vocal)){
                         return vocal.replace(vocal,'I');
                    }else if(conAcentoMinus.test(vocal)){
                         return vocal.replace(vocal,'í');
                    }else if(conAcentoMayus.test(vocal)){
                         return vocal.replace(vocal,'Í');
                    }
               }
               strChange.push(changeVocal(strInicial[i]));
          }else{
               strChange.push(strInicial[i]);
          }
     } 
     strFinal = strChange.join('');
     return strFinal;
}


export function contarA(strWithA) { 

     //const vocalMinus = 'a';
     //let sumaOcurrencias = 0;
     //let indxVocalStrEntrada = strEntrada.indexOf(vocalMinus);
     const regSearchA = /[aáÁA]/g;
     let foundA = strWithA.match(regSearchA);

     if(foundA === null){
          return 0;
     }else{
          return foundA.length;
     }
     
     // while ( indxVocalStrEntrada != -1) {
     //     sumaOcurrencias++;
     //     indxVocalStrEntrada = strEntrada.indexOf(vocalMinus, indxVocalStrEntrada + 1);
     //}
}


export function esPalindromo(strAnalizar) {
     
     //Method 1   
     let arrStrAnalizar;
     let arrStrAnalizarReverse;
     let reverseString;

     if(typeof strAnalizar == 'string'){

          arrStrAnalizar = strAnalizar.split("");
          arrStrAnalizarReverse = strAnalizar.split("").reverse();

          let checkArrPalindromo = ( arrA, arrB ) => arrA.every((element,index) => element == arrB[index]);

          //return checkArrPalindromo(arrStrAnalizar,arrStrAnalizarReverse);

     }else{
          throw new Error ('Se ha introducido un dato de tipo Number');
     }

     //---------------------------------FIN---------------------------------------//

     //Method 2
     
     if(typeof strAnalizar == 'string'){
          
          reverseString = strAnalizar.split("").reverse().join("");

          if(strAnalizar === reverseString){
               return true; 
          }else{
               return false;
          }
     }else{
          throw new Error ('Se ha introducido un dato de tipo Number');
     }

     
 }



export function esPrimo(supuestoPrimo){

     let generarArrPrimos = (n) =>{
          let allPrimos = [];
          let findPrimos = [];

          if( typeof n === 'number' && Number.isInteger(n) ){
               if(n >= 2){
                    for(let i=0 ; i <= n; i++){
                         allPrimos.push(true);
                    }
                    
                    for(let i=2 ; i <= n ; i++){
                         if(allPrimos[i]){
                              findPrimos.push(i);
                              for(let j = 1 ; j * i <= n; j++){
                                   allPrimos[j*i] = false;
                              }
                         }
                    }
                    return findPrimos;
               } else {
                    throw new Error("El numero debe ser mayor o igual a 2");
               }
          }
     }; 
     
     switch (typeof supuestoPrimo) {

          case "number":
               
          if(supuestoPrimo == 2){
               return true;
          }else{

               const menorCociente = (nPrimoRange) => Math.floor(supuestoPrimo / nPrimoRange) <= nPrimoRange; 
               const restoCero = (nPrimoRange) => supuestoPrimo % nPrimoRange == 0 ;

               let primosRange = generarArrPrimos(supuestoPrimo);
               let checkMenorCociente = primosRange.some(menorCociente);
               let checkIndxMenorCociente = primosRange.findIndex(menorCociente);
               let checkCeroResto = primosRange.some(restoCero);
               let checkIndxZeroResto = primosRange.findIndex(restoCero);             
                             
               if(checkMenorCociente && checkCeroResto && checkIndxZeroResto <= checkIndxMenorCociente){
                    return false;
               }else{
                    return true;
               }
          }
          
          case "string":
               return undefined;
          case "object":
               return undefined;
          case "undefined":
               throw new Error('No puedes lanzar la función vacia');
    }

 }